# Traexport
## Français
### Présentation
Ce logiciel permet d'utiliser les fonctionnalités de génération de certificats et de renouvellement de [Traefik](https://github.com/traefik/traefik) afin de fournir des certificats au format pem, soit directement dans un container, soit dans un "Volume Docker"/"Bind Mounts" (idéalement partagé avec un container). Toutes ces extractions sont repérés par les labels sur les containers déployer.

Pour effectuer ces opérations le logiciel se connecte à l'API docker (rw) et doit être en mesure de lire le fichier acme.json de traefik (ro)

### Exemple d'utilisation
L'utilisation classique passe le déploiement via docker-compose
```yaml
version: "3.7"
services:
  traexport:
    image: registry.gitlab.com/nonot/traexport
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
```

et les services cibles avec des labels, exemple:
```yaml
version: "3.7"
services:
  whoami:
    image: traefik/whoami
    command:
       - --cert=/etc/yourdomain.tld.crt
       - --key=/etc/yourdomain.tld.key
    labels:
      - traexport.enable=yes
      - traexport.domain=yourdomain.tld
      - traexport.container_path=/etc/
      - traexport.restart=yes
    ports:
      - 8081:80
``` 

Dans cet exemple fonctionnel dès que `whoami` démarrera, `traexport` utilisera le premier certificat dont de le `domain.main` correspond à `yourdomain.tld`, appliquera les metadatas par défaut au certificat et à la clé privé et l'enverra dans le container sous le chemin `/etc/`. (i.e. `/etc/yourdomain.tld.crt` and `/etc/yourdomain.tld.key`)

Aussi si le certificat de `yourdomain.tld` venait à changer, `traexport` le poussera dans le container `whoami` et le redémarrera



### Labels disponibles

| nom                      | requis | valeur **par défaut** | description |
|---------------           |--------|-----------------------|-------------|
| traexport.enable         | non    | yes or **no**         | active ou non l'export pour ce container | 
| traexport.domain         | oui    |                       | le domaine qui sera recherché dans le fichier acme.json |
| traexport.local_path     | non    |                       | le chemin d'export **dans le contexte de traexport** (1) |
| traexport.container_path | non    |                       | le chemin d'export dans le container |
| traexport.cert_file_name | non    | **{domain}.crt**      | le nom du fichier du certificat |
| traexport.key_file_name  | non    | **{domain}.key**      | le nom du fichier de la clé privé |
| traexport.owner          | non    | **0**                 | l'uid et le gid des certificats | 
| traexport.perms          | non    | **0600**              | le niveau de permission sur les certificats au format '**0**XXX' | 
| traexport.restart        | non    | yes or **no**         | est-ce que le container doit redémarrer si les certificats sont manquants ou incorrectes ? | 

note: local_path et container_path ne sont ni requis, ni exclusif. Si aucun n'est défini aucun export ne sera fait, si les deux le sont les deux seront effectués. 

(1) c'est-à-dire que traexport utilisera ce chemin dans son propre filesystem, cela est utile si un volume est monté à ce chemin afin de pouvoir le récupérer et/ou le partager avec un autre container.

### Options Docker
You can connect to Docker by socket (default) or specify options to use http protocol.

Use the following environment vars to configure client: //not tested yet
 * DOCKER_HOST
 * DOCKER_PORT
 * DOCKER_CA
 * DOCKER_CERT
 * DOCKER_KEY 

### Limitations connus
 * ne génére qu'au format .pem
 * sélection minimaliste du certificat, un domaine -> une correspondance

### Améliorations futurs
 * implentation des variables d'environnement pour utiliser tout les types de connexion de la librairie [dockerode](https://github.com/apocas/dockerode)
 * configuration par fichier dynamique YAML (similaire au dynamic config de traefik)
