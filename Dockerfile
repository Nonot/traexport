FROM node:17 as base
WORKDIR /src/

FROM base as dev
RUN npm install --global nodemon
CMD nodemon --inspect=0.0.0.0 code/main.mjs 

FROM base as prod
ADD src/ /src/
RUN npm ci
CMD [ "/usr/local/bin/node", "/src/code/main.mjs" ]
