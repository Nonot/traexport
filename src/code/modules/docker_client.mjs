import Docker from "dockerode";
import { write_cert } from "./write_certs.mjs";
import tar from "tar";
import { rmdir, readFileSync, statSync, chownSync, chmodSync } from "fs";
import { mkdtemp } from "fs/promises";
import path from "path";
import os from "os";
import _ from "lodash";

const docker = new Docker();

const extract_labels = (config, path_labels, id_path) => {
  const getLabels = _.property(path_labels);
  const getId = _.property(id_path);
  const labels = _.map([config], getLabels)[0];
  const id = _.map([config], getId)[0];
  return labels["traexport.enable"] === "yes"
    ? {
        container_path: labels["traexport.container_path"],
        local_path: labels["traexport.local_path"],
        domain: labels["traexport.domain"],
        perms: labels["traexport.perms"] || "0600",
        owner: Number(labels["traexport.owner"]) || 0,
        restart: labels["traexport.restart"] === "yes" ? true : false,
        id: id,
        cert_file_name:
          labels["traexport.cert_file_name"] ||
          `${labels["traexport.domain"]}.crt`,
        key_file_name:
          labels["traexport.key_file_name"] ||
          `${labels["traexport.domain"]}.key`,
      }
    : false;
};

const save_as_tar = async (config, { cert, key }, domain) => {
  const cert_path = "./" + config.cert_file_name;
  const key_path = "./" + config.key_file_name;
  const tar_path = `./${domain}.tar`;
  await write_cert(cert_path, cert);
  await write_cert(key_path, key);
  chmodSync(cert_path, config.perms);
  chownSync(cert_path, config.owner, config.owner);
  chmodSync(key_path, config.perms);
  chownSync(key_path, config.owner, config.owner);
  return tar.c({ gzip: false, file: tar_path }, [cert_path, key_path]);
};

function compare_container_file(container_result, data_to_write, config) {
  const end_content = container_result.indexOf(
    "\x00",
    container_result.length - 512 * 3
  );
  const file = {
    name: container_result.slice(0, 100).toString(),
    perms: container_result.slice(103, 107).toString(),
    uid: Number(container_result.slice(108, 115)).toString(),
    gid: Number(container_result.slice(116, 123)).toString(),
    content: container_result.toString("ascii", 512, end_content),
  };
  if (file.content !== data_to_write) {
    console.info("Content in", file.name, "is incorrect");
    return false;
  }
  if (file.perms !== config.perms) {
    console.info("Permissions of", file.name, "are incorrect");
    return false;
  }
  if (file.uid != config.owner.toString(8)) {
    console.info("User owner of", file.name, "is incorrect");
    return false;
  }
  if (file.gid !== config.owner.toString(8)) {
    console.info("Group owner of", file.name, "is incorrect");
    return false;
  }
  return true;
}
async function save_to_container(target, config, certificates) {
  const curCwd = process.cwd();
  const directory = await mkdtemp(path.join(os.tmpdir() + "/"));
  process.chdir(directory);

  await save_as_tar(
    config,
    { cert: certificates.cert, key: certificates.key },
    config.domain
  );

  try {
    const incoming_message_certificate = await target.getArchive({
      path: path.join(config.container_path, config.cert_file_name),
    });
    const certificate_content = await new Promise((resolve, reject) => {
      incoming_message_certificate.on("data", resolve);
      incoming_message_certificate.on("error", reject);
    });
    const is_certificate_uptodate = compare_container_file(
      certificate_content,
      certificates.cert,
      config,
      target
    );
    const incoming_message_key = await target.getArchive({
      path: path.join(config.container_path, config.key_file_name),
    });
    const key_content = await new Promise((resolve, reject) => {
      incoming_message_key.on("data", resolve);
      incoming_message_key.on("error", reject);
    });
    const is_key_uptodate = compare_container_file(
      key_content,
      certificates.key,
      config,
      target
    );
    if (is_certificate_uptodate && is_key_uptodate) {
      process.chdir(curCwd);
      rmdir(directory, () => {});
      return true;
    }
    await target.putArchive(`${config.domain}.tar`, {
      path: config.container_path,
    });
    console.error("incorrect certificate or key, copy");
    process.chdir(curCwd);
    rmdir(directory, () => {});
    return false;
  } catch (err) {
    await target.putArchive(`${config.domain}.tar`, {
      path: config.container_path,
    });
    console.error("missing certificate or key, copy");
    process.chdir(curCwd);
    rmdir(directory, () => {});
    return false;
  }
}

async function save_update_file(content, config, dest) {
  const stat = statSync(dest, { throwIfNoEntry: false });

  let upToDate = true;
  if (stat === undefined) {
    await write_cert(dest, content);
    chmodSync(dest, config.perms);
    chownSync(dest, config.owner, config.owner);
    upToDate = false;
  } else {
    const cur_content = readFileSync(dest).toString();
    if (cur_content !== content) {
      upToDate = false;
      await write_cert(dest, content);
    }
    if (stat.uid !== config.owner || stat.gid !== config.owner) {
      console.info("Owner/group update for", dest, config.id, config.domain);
      chownSync(dest, config.owner, config.owner);
      upToDate = false;
    }
    const cur_perms = "0" + (stat.mode & parseInt("777", 8)).toString(8);
    if (cur_perms !== config.perms) {
      chmodSync(dest, config.perms);
    }
  }
  return upToDate;
}
async function save_to_local(config, { cert, key }) {
  let upToDate = true;
  const dest_cert = path.join(config.local_path, config.cert_file_name);
  const dest_key = path.join(config.local_path, config.key_file_name);

  if (!save_update_file(cert, config, dest_cert)) {
    upToDate = false;
    console.info(`Local: ${dest_cert} overwritten`);
  }
  if (!save_update_file(key, config, dest_key)) {
    upToDate = false;
    console.info(`Local: ${dest_key} overwritten`);
  }
  return upToDate;
}

const process_container = async (config, acme_store) => {
  const target = docker.getContainer(config.id);
  const certificates = acme_store.get_domain_certificate(config.domain);
  if (!certificates) {
    console.warn(
      `No certificates for ${config.domain}, cancelling ${target.id}`
    );
    return;
  }
  const is_container_uptodate = config.container_path
    ? await save_to_container(target, config, certificates)
    : true;
  const is_local_uptodate = config.local_path
    ? await save_to_local(config, certificates)
    : true;
  const is_uptodate = is_container_uptodate && is_local_uptodate;
  if (config.restart && !is_uptodate) {
    console.info(`Restarting ${config.id}`);
    target.restart();
  }
};

export const listen_docker_events = async (acme_store) => {
  docker.getEvents({}, (err, stream) => {
    if (err || !stream) {
      console.error(`Failed to monitor host: ${err}`);
      return;
    }
    console.info("Docker client listenning: OK");
    stream.on("data", async (event) => {
      event = JSON.parse(event);
      if (event.status == "create") {
        const config = extract_labels(event, "Actor.Attributes", "Actor.ID");
        if (config) {
          console.info(
            `Start of container ${event.Actor.Attributes.name}: ${config.domain}`
          );
          process_container(config, acme_store);
        }
      }
    });
  });
};

export const process_all_containers = async (acme_store) => {
  const containersList = await docker.listContainers();
  const configs = containersList
    .map((container) => extract_labels(container, "Labels", "Id"))
    .filter((config) => config);
  for (const config of configs) {
    process_container(config, acme_store);
  }
};

export const update_container_per_domain = async (domain, acme_store) => {
  const containersList = await docker.listContainers();
  const concernedContainers = containersList
    .map((container) => extract_labels(container, "Labels", "Id"))
    .filter((config) => config.domain === domain);
  for (const config of concernedContainers) {
    console.info(
      `Install new certificates for ${config.id}, ${config.domain}`
    );
    process_container(config, acme_store);
  }
};
