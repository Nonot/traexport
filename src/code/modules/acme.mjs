import fs from "fs";
import { readFile } from "fs/promises";
import * as R from "ramda";
import { update_container_per_domain } from "./docker_client.mjs";

function decode_certificate({ domain, certificate, key }) {
  const certBuffer = new Buffer.from(certificate, "base64");
  const decCert = certBuffer.toString("ascii");
  const keyBuffer = new Buffer.from(key, "base64");
  const decKey = keyBuffer.toString("ascii");
  return { cert: decCert, key: decKey, domain };
}

function difference_between_acmes(old_acme, new_acme) {
  const old_certificates = Object.values(old_acme)
    .map((provider) => {
      return provider.Certificates;
    })
    .flat();
  const new_certificates = Object.values(new_acme)
    .map((provider) => {
      return provider.Certificates;
    })
    .flat();
  const diff = R.difference(new_certificates, old_certificates);
  return diff;
}

async function read_acme_file(acme_path) {
  return JSON.parse(await readFile(acme_path));
}

function prepare_for_update_store(new_acme, old_data) {
  const cert_diffs = difference_between_acmes(old_data, new_acme);
  return cert_diffs.map((cert) => {
    return decode_certificate(cert);
  });
}
export class Acme_Watch {
  acme_path = "";
  file_content = { nothing: { Certificates: [] } };
  acme_store;
  constructor(acme_path, acme_store) {
    this.acme_path = acme_path || "/mnt/acme.json";
    this.acme_store = acme_store;
  }
  async start() {
    const acme_content = await read_acme_file(this.acme_path);
    const dec_cert_diffs = prepare_for_update_store(
      acme_content,
      this.file_content
    );
    dec_cert_diffs.forEach((cert) => {
      this.acme_store.update_certificates(cert);
    });
    this.file_content = acme_content;
    return this.watch_acme(this.acme_store);
  }
  async watch_acme() {
    fs.watchFile(this.acme_path, {}, async (event) => {
      if (event.ino === 0) {
        console.warn("Acme file does not exist (anymore)");
        return;
      }
      if (event.birthtime === event.ctime) {
        console.info("Acme file has been created");
        return;
      }
      if (event.birthtime !== event.mtime) {
        console.info("Acme file has been updated");
        const acme_content = await read_acme_file(this.acme_path);
        const dec_cert_diffs = prepare_for_update_store(
          acme_content,
          this.file_content
        );
        dec_cert_diffs.forEach((cert) => {
          this.acme_store.update_certificates(cert);
          update_container_per_domain(cert.domain.main, this.acme_store);
        });
        this.file_content = acme_content;
      }
    });
    console.info("Watching acme file: OK");
  }
}

export class Acme_Storage {
  certificates = {};
  update_certificates(decCert) {
    this.certificates[decCert.domain.main] = decCert;
  }
  get_domain_certificate(domain) {
    return this.certificates[domain];
  }
}
