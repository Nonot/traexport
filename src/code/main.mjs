import {
  listen_docker_events,
  process_all_containers,
} from "./modules/docker_client.mjs";
import { Acme_Watch, Acme_Storage } from "./modules/acme.mjs";

const ACME_PATH = process.env.ACME_PATH || "/mnt/acme.json";

const main = () => {
  const acme_store = new Acme_Storage();
  const acme_watcher = new Acme_Watch(ACME_PATH, acme_store);
  acme_watcher.start().then(() => {
    listen_docker_events(acme_store);
    process_all_containers(acme_store);
  });
};
setTimeout(() => {
  main();
}, 0); // pour avoir le temps d'attacher le debugger
process.on("SIGTERM", () => {
  console.info("Stopping program...");
  process.exit();
});
